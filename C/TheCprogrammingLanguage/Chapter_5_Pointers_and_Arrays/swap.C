#include <stdio.h>

void swap(int *px, int *py);

int main() {
    int a = 2, b = 1;

    printf("a is %d and b is %d\n", a, b);

    swap(&a, &b);

    printf("a is %d and b is %d\n", a, b);
}
void swap(int *px, int *py)
{
    int temp;

    temp = *px;
    *px = *py;
    *py = temp;
}
