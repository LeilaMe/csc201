#include <stdio.h>

int main() {
    int x = 1, y = 2, z[10];
    int *ip, *iq;

    printf("x is %d y is %d z[0] is %d\n", x, y, z[0]);
    ip = &x;
    printf("x is %d y is %d z[0] is %d\n", x, y, z[0]);
    y = *ip;
    printf("x is %d y is %d z[0] is %d\n", x, y, z[0]);
    *ip = 0;
    printf("x is %d y is %d z[0] is %d\n", x, y, z[0]);
    ip = &z[0];
    printf("x is %d y is %d z[0] is %d\n", x, y, z[0]);

    *ip = *ip + 10;
    printf("x is %d y is %d z[0] is %d\n", x, y, z[0]);
    y = *ip + 1;
    printf("x is %d y is %d z[0] is %d\n", x, y, z[0]);
    *ip += 1;
    printf("x is %d y is %d z[0] is %d\n", x, y, z[0]);
    ++*ip;
    printf("x is %d y is %d z[0] is %d\n", x, y, z[0]);
    (*ip)++;
    printf("x is %d y is %d z[0] is %d\n", x, y, z[0]);
    iq = ip;
    printf("x is %d y is %d z[0] is %d\n", x, y, z[0]);
}
