#include <stdio.h>

/* copy input to output; 2nd version */
int main() {
    int c;
    int i = getchar() != EOF;

    while ((c = getchar()) != EOF) {
        putchar(c);
        printf("\nc: %d\n", c);
        printf("EOF: %d\n", EOF);
        printf("getchar() != EOF: %d\n", i);
    }
}
