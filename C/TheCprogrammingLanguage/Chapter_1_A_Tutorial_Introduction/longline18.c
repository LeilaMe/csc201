#include <stdio.h>
#include "../../../../acctools.h"
#define MAXLINE 1000 /* maximum input line size */

/*int gtline(char line[], int maxline);  change name from getline to gtline */
void copy(char to[], char from[]);

/* print longest input line */
int main() {
    int len; /* current line length */
    int max; /* maximum length seen so far */
    char line[MAXLINE]; /* current input line */
    char longest[MAXLINE]; /* longest line saved here */

    max = 0;
    /*while ((len = accgetline(line, MAXLINE)) > 0)
        if (len > max) {
            max = len;
            copy(longest, line);
        }*/
    while ((len = accgetline(line, MAXLINE)) > 0)
        copy(longest, line);

    /* if (max > 0)  there was a line 
        printf("length: %d %s", max, longest);*/
    return 0;
}

/* getline: read a line into s, return length */
int gtline(char s[], int lim){
    int c, i;

    for (i = 0; i < lim - 1 && (c=getchar())!=EOF && c!='\n'; ++i)
        s[i] = c;
    if (c == '\n') {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;
}

/* copy: copy 'from' into 'to'; assume to is big enough */
void copy(char to[], char from[]){
    int i;
    char c;
    char n;

    i = 0;
    while ((to[i] = from[i]) != '\0'){
        printf("%c", from[i]);
        while ((c = getchar()) != EOF){
           if (c != ' ' && c!= '\n' && c!= '\t')
               continue;
           else
               ++n;
        ++i;
        }
    }
}
