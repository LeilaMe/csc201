#include <stdio.h>

int temp(int l, int u, int s);

int main() {
    temp(0, 300, 20);
}

int temp(int low, int upp, int step)
{
    int fa, ce;
    
    fa = low;
    while (fa <= upp) {
        ce = 5 * (fa - 32) / 9;
        printf("%d\t%d\n", fa, ce);
        fa = fa + step;
    }
    return 0;
}
