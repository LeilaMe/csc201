#include <stdio.h>

/* count blanks, tabs, and newlines in input */
int main() {
    int c, nl, nt, nb;

    nl = 0;
    nt = 0;
    nb = 0;

    while ((c = getchar()) != EOF){
        if (c == '\n')
            ++nl;
        if (c == '\t')
            ++nt;
        if (c == '\b')
            ++nb;
    }
    printf("%d\n", nl);
}
