#include <stdio.h>

#define IN 1
#define OUT 0
#define rows 46

/* print a histogram of the lengths of words in its input. horizontal */
int main() {
    int c, i, len, state;
    len = 0;
    int alengths[rows];

    for (i = 0; i < rows; ++i)
        alengths[i] = 0;
    
    state = OUT;
    while ((c = getchar()) != EOF){
        if (c == ' ' || c == '\n' || c == '\t'){
            state = OUT;}
        else if (state == OUT) {
            state = IN;
            ++alengths[len];
            len = 0;
        }
        else
            ++len;
    }
    for (i = 0; i < rows; ++i)
        printf("%d letters: %d\n", i+1, alengths[i]);
}
