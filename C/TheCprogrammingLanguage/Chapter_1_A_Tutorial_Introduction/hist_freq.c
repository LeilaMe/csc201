#include <stdio.h>

/* prints a hitogram of frequencies of different characters */
int main() {
    int c, i;
    int ndigit[10];
    int ldigit[26];
    int udigit[26];

    for (i = 0; i < 10; ++i)
        ndigit[i] = 0;
    for (i = 0; i < 26; ++i){
        ldigit[i] = 0;
        udigit[i] = 0;
    }

    while ((c = getchar()) != EOF)
        if (c >= '0' && c <= '9')
            ++ndigit[c-'0'];
        else if (c >= 'a' && c <= 'z')
            ++ldigit[c-'a'];
        else if (c >= 'A' && c <= 'Z')
            ++udigit[c-'A'];

    for (i = 0; i < 10; ++i)
        printf("%d: %d\n", i, ndigit[i]);
    printf("lowercase letters\n");
    for (i = 0; i < 26; ++i)
        printf("%d: %d\n", i, ldigit[i]);
    printf("uppercase letters\n");
    for (i = 0; i < 26; ++i)
        printf("%d: %d\n", i, udigit[i]);

    return 0;
}
