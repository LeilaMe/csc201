#include <stdio.h>

/* copy input to output and replace strings of one or more
blanks by a single blank */
int main() {
    int c, nb;

    nb = 0;
    while ((c = getchar()) != EOF)
        if (c == '\b')
            c = '\b';
    printf("%d%d\n", c, nb);
}
