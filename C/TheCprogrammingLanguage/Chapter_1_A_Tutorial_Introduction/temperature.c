#include <stdio.h>

/*print Fahrenheit-Celsius table for fahr = 0, 20 ..., 300 */
int main(){
    int fahr, celsius;
    int lower, upper, step;

    lower = 0;
    upper = 300;
    step = 20;

    printf("temperature conversion table\n");

    fahr = lower;
    while (fahr <= upper) {
        celsius = 5 * (fahr-32) / 9;
        printf("%d\t%d\n", fahr, celsius);
        fahr = fahr + step;
    }

    printf("Celsius to Fahrenheit\n");
    celsius = lower;
    while (celsius <= upper) {
        fahr = 9 * celsius / 5 + 32;
        printf("%3d\t%3d\n", celsius, fahr);
        celsius += step;
    }
}
