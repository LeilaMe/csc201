#include <stdio.h>

int main()
{
    printf("hi, it's me\n");
    printf("hello, ");
    printf("continued");
    printf("\n");
    printf("\t\\o/");
    printf("\b");
    printf("\n");
}

/*
   no first line
   include the header <stdio.h> or explicitly provide a declaration for 'printf'
   no int
   type specifier missing
   no main()
   expected identifier or '('
   no f
   implicit declaration of function 'print' is
      invalid in C99
      
   no ;
   expected ';' after expression
   \c
   unknown escape sequence '\c'
   letters that do no work
   q w g y u i o p s d h j k l z x c m
   letters that compile
   e r t a 
   letters that do something
   f - a full line?
   v - also a full line?
   b - idk, book says backslash
*/
