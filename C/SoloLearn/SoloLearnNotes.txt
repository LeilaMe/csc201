general-purpose programming language
50 years
used in:
operating systems
complex programs
header file= required file for a function like printf

four data types: 
int
float
double
char

no boolean D:
or String

arguments in printf separated by comma
format specifier or conversion specifiers (%ld long decimal). it gets replaced by the value of the second argument
sizeof() gives value of size in bytes for the data type. 

printf can have multiple format specifiers with corresponding arguments to replace the specifiers.

A variable is a name for an area in memory.

variable names (identifier)
begin with letter / underscore
composed of letters, digits, and the underscore character
common: lowercase words with underscores - snake_case

variables musst be declared as a data type before used

change the value of a declared variabel with an assignment statement. Declare and initialize (assign initial value) in one statement.

declare multiple vairables on one line by separating with a comma

format specifiers:
%ld long decimal
%d an int?
%f float
%c char

C is case-sensitive.

Constants store values that cannot be changed from its initial assignment. 
make code easy to read and understand by naming constants meaningfully. 

COMMON PRACTICE:
use uppercase identifiers for constants.

one way to define a constant is using the const keyword in a variable declaration. 
the value of the constant cannot be changed during the program execution. For example, trying to assign it another value will generate an error. 

Constants must be initialized when declared. 

another way to define a constant is with the #define preprocessor directive. it uses macros for defining constant values. 
Before compilation, the preprocessor replaces all macro identifiers in the code with its corresponding values from the directive. 
The final code sent to the compiler will already have the constant values in place. 

const uses memory for storage and #define does not

no semicolons at the end of #define statements. no equals sign either...

INPUT

getchar() returns value of the next single character input
the input is stored in the variable a

gets() function reads input as an ordered sequence of characters, called a string. 
A string is stored in a char array
according to my terminal this program is unsafe, also it returned abort:6? idk

it's supposed to store the input in an array of 100 characters. 

scanf() scans input that matches format specifiers. 
the & sign before the variable name is the address operator
it gives the address (location in memory) of a variable. 
scanf places an input value at a variable address.
it printed numbers
ohh it has to match the format. I put in letters and in the terminal it gave back a bunch of numbers but it sololearn it geve back 0
then when i inputed 1234 it returned 1234

scanf() stops reading as soon as it encounters a space, so text such as "Hello World" is two separate inputs for scanf().

OUTPUT
other than printf() function

putchar() outputs a single character
true it does
it does that 
yup

again the input is stored in variable a

the puts() function displays output as a string
a string is still stored in a char array
wow it returned both words
it even says the gets() function is dangerous in sololearn bruh

We stored the input in an array of 100 characters. I feel like we did that. 

scanf() scans ok this is just repeating. 
proceeds to display the scan code again. or scan program? idk
same info about the & sign. and the sum code. Maybe this was an accident. 

FORMATTED INPUT

scanf() assigns input to variables. yeah. It scans the input according to format specifiers that convert input as necessary. If input can't be converted, then the assignment isn't made. The scanf() statement waits for input and then makes assignments. 
summary of that: scanf() scans the input and according to format specifiers it converts and assigns it to the variable. 

i have no idea how to get this example to work
oh wait there is no output lemme do again
i did it

& isn't needed for strings because the string name is the pointer

format specifier
%[*][max_field]conversion character
The optional * will skip the input field.
The optional max_width gives the maximum number of characters to assign to an input field.
The conversion character converts the argument, if necessary, to the indicated type:
d decimal
c character
s string
f float
x hexadecimal

FORMATTING OUTPUT

a call to the printf function requires a format string. 
This format string can include escape sequences for outputting special characters and format specifiers that are replaced by variables. 

Escape sequences begin with a backslash \:
\n new line
\t horizontal tab
\\ backslash
\b backspace
\' single quote
\" double quote

Format specifiers begin with a percent sign % and are replaced by corresponding arguments after the format string. 

%[-][width].[precision]conversion character
The optional - specifies left alignment of the data in the string.
The optional width gives the minimum number of characters for the data.
The period . separates the width from the precision.
The optional precision gives the number of decimal places for numeric data. If s is used as the conversion character, then precision determines the number of characters to print.
The conversion character converts the argument, if necessary, to the indicated type:
d decimal
c character
s string
f float
e scientific notation
x hexadecimal

To print the % symbol, use %% in the format string.

COMMENTS

benefit the reader
compiler ignores comments
starts with a slash asterisk /* and ends with an asterisk slash */ and can be anywhere in your code.
same line as a statement, or they can span several lines.

according to comments: nesting comments will cause compiler error

comments clarify the program's intent to the reader. 
Use comments to clarify the purpose and logic behind segments of code.

Single-line Comments

C++ introduced a double slash comment // as a way to comment single lines. 
Some C compilers also support this comment style.

good programming practice
facilitates a clear understanding of the code for you and for others who read it

OPERATORS

C supports arithmetic operators + (addition), - (subtraction), * (multiplication), / (division), and % (modulus division).

operators used in numeric expression such as 10 + 5
that has two operands and the addition operator
numeric expressions used in assignment statements

Division

C has two division operators: / and %.

int data types, integer division, also called truncated division, removes any remainder to result in an integer
real numbers (float or double), the result is a real number

% operator returns only the remainder of integer division
useful for many algorithms, including retrieving digits from a number. 
Modulus division cannot be performed on floats or doubles

Operator Precedence

C evaluates a numeric expression based on operator precedence.
+ and – are equal in precedence
also *, /, and %

*, /, and % are performed first in order from left to right and then + and -, also in order from left to right.

You can change the order of operations by using parentheses ( ) to indicate which operations are to be performed first.
For example, the result of 5 + 3 * 2 is 11, where the result of (5 + 3) * 2 is 16.

C may not evaluate a numeric expression as desired when the associative property allows any order. For example, x*y*z may be evaluated as (x * y) * z or as x * (y * z). If order is important, break the expression into separate statements.

Type Conversion

When a numeric expression contains operands of different data types, they are automatically converted as necessary in a process called type conversion.

For example, in an operation involving both floats and ints, the compiler will convert the int values to float values.

When you want to force the result of an expression to a different type you can perform explicit type conversion by type casting

Explicit type conversion, even when the compiler may do automatic type conversion, is considered good programming style.

Assignment Operators

An assignment statement evaluates the expression on the right side of the equal sign first and then assigns that value to the variable on the left side of the =. This makes it possible to use the same variable on both sides of an assignment statement, which is frequently done in programming.

C offers the += assignment operator
Many C operators have a corresponding assignment operator
x *= 1; 

The entire expression on the right is evaluated and then added to x before being assigned to x. You can think of the statement as x = x + (3 * 2).

Increment & Decrement

Adding 1 to a variable can be done with the increment operator ++. Similarly, the decrement operator -- is used to subtract 1 from a variable.

The increment and decrement operators can be used either prefix (before the variable name) or postfix (after the variable name)

Which way you use the operator can be important in an assignment statement

The prefix form increments/decrements the variable and then uses it in the assignment statement.
The postfix form uses the value of the variable first, before incrementing/decrementing it.

