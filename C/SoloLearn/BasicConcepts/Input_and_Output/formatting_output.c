#include <stdio.h>

int main() {
    printf("The box has %d flowers.\n", 21);
    /* The box has 21 flowers. */

    printf("\"Hello World!\"\n");
    /* "Hello World" */
}
