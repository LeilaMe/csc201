#include <stdio.h>

int main() {
    int x = 42; //int for a whole number

    //%d is replaced by x
    printf("%d \n", x);

    return 0;
}
