#include <stdio.h>

int main() {
    float average;
    int total = 23;
    int count = 4;

    average = (float) total / count;
    /* average = 5.75 */

    printf("%4.2f \n", average);
    /* Without the type casting, average will be assigned 5 */

    return 0;
}
