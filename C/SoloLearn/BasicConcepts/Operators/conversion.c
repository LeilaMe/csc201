#include <stdio.h>

/* In the following program, the increase variable is automatically converted to a float */

int main() {
    float price = 6.50;
    int increase = 2;
    float new_price;

    new_price = price + increase;
    printf("New price is %4.2f \n", new_price);
    /* Output: New price is 8.50 */

    return 0;
}
