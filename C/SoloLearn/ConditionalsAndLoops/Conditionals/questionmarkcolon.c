#include <stdio.h>

int main() {
    int a;
    int b = 7;

    a = (b >= 10) ? 9000 : b;
/* assign a 9000 or b depending on (b >= 10) */
    /* This is equivalent to
       if (b >= 10) 
         a = 5;
       else
         a = b;  */
    printf("%d\n", a);
    return 0;
}
