#include <stdio.h>

int main() {
    int num = 4;
    
    switch (num) {
        case 1:
        case 2:
        case 3:
            printf("ONE, TWO, or THREE.\n");
            break;
        case 4:
        case 5:
        case 6:
            printf("FOUR, FIVE, or SIX.\n");
            break;
        default:
            printf("Greater than six.\n");
    }
}
