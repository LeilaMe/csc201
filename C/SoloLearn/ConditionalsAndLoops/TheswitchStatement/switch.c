#include <stdio.h>

int main() {
    int num = 3;

    switch (num) {
        case 1:
            printf("ONE\n");
            break;
        case 2:
            printf("TWO\n");
            break;
        case 3:
            printf("THREEE\n");
            break;
        default:
            printf("not 1, 2, or 3.\n");
    }
}
