#include <stdio.h>
#include "../../../acctools.h"

int main() {
    //char name[5] = "Leila";
    
    char alph[26] = "abcdefghijklmnopqrstuvwxyz";
    int out;
    char got;

    printf("The string: %s\n", alph);

    printf("input one letter: \n");
    got = getchar();

    //printf("%c\n", got);

    out = strindex(alph, &got);
    printf("The letter %c is at position %d \n", got, out+1);
    return 0;
}
