#include <stdio.h>
#include "../../../acctools.h"

/* count binary "bytes" */
int main() {
    char c;
    int b = 0;
    char search[1] = {" "};
    
    while ((c = getchar()) != EOF) {
        if ((strindex(&c, search)) != -1){
            b++;
            printf("%d\n", b);
        }
    }
    printf("there are %d \n", b);

    return 0;
}
