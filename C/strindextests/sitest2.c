/* letter shift */
#include <stdio.h>
#include "../../../acctools.h"

int gtline(char line[], int maxline); /* change name from getline to gtline */

int main() {
    char find;
    char replace;
    int c, o;
    char new[1000];
    char line[1000];
    char alph[27] = "abcdefghijklmnopqrstuvwxyza";

    while ((find = getchar()) != EOF) {
        o = strindex(alph, &find);
        if (o == ' '){
            printf(" ");
            break;
        }
        printf("%c", alph[o+1]);
    }
    printf("\n");

    return 0;
}
/* getline: read a line into s, return length */
int gtline(char s[], int lim){
    int c, i;

    for (i = 0; i < lim - 1 && (c=getchar())!=EOF && c!='\n'; ++i)
        s[i] = c;
    if (c == '\n') {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;
}
