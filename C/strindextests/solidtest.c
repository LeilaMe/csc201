#include <stdio.h>
#include "../../../acctools.h"

int main(){
    char s[] = "cabbage\0";
    char t1[] = "bag\0";

    printf("%s\n", s);
    printf("index of %s: %d\n", t1, strindex(s, t1));

    char t2[] = "cab\0";
    printf("index of %s: %d\n", t2, strindex(s, t2));

    char t3[] = "age\0";
    printf("index of %s: %d\n", t3, strindex(s, t3));

    char t4[] = "cabbage\0";
    printf("index of %s: %d\n", t4, strindex(s, t4));

    char t5[] = "no\0";
    printf("index of %s: %d\n", t5, strindex(s, t5));

    char t6[] = "cabbages!\0";
    printf("index of %s: %d\n", t6, strindex(s, t6));

    s[2] = 'x';
    s[7] = 's';
    s[8] = '\0';

    printf("%s\n", s);
    return 0;
}
