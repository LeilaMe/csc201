package com.hfad.randompairchooser;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RandomPairChooser extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activityrandompairchooser);
    }
    public void onClickPairUp(View view) {
        TextView pair;
        pair = findViewById(R.id.pair);
        EditText namesView = findViewById(R.id.names);
        String apair = namesView.getText().toString();

        String[] namesList = apair.split(" ");
        List<String> stringList = Arrays.asList(namesList);
        Collections.shuffle(stringList);
        stringList.toArray(namesList);

        ArrayList<String> newString = new ArrayList<>();

        if (namesList.length % 2 == 0){
            for (int i = 0; i < namesList.length; i = i + 2) {
                newString.add(namesList[i] + " and " + namesList[i + 1]);
            }
        } else {
            for (int i = 0; i < namesList.length-1; i = i + 2) {
                newString.add(namesList[i] + " and " + namesList[i + 1]);
            }
            newString.add(namesList[namesList.length-1]);
        }

        pair.setText(newString.toString());
    }
}